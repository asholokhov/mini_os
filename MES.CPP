#include <dos.h>
#include <conio.h>
#include <stdio.h>
#include <string.h>

#include "mini_os\mes.h"
#include "mini_os\man.h"
#include "mini_os\map.h"
#include "mini_os\sem.h"

void SendMessage(const char* pname, const Msg &msg) {
	HANDLE h;
	MBeginEnumerate();
	while (MEnumerateHandles(h)) {
		if (strncmp(h->name, pname, TASK_NAME_LEN) == 0) {
			h->msgs.Push(msg);
		}
	}
}

void SendMessage(HANDLE handle, const Msg &msg) {
	handle->msgs.Push(msg);
}

unsigned GetMessage(Msg &msg) {
	HANDLE h = MGetProcHandle();
	if (!h) {
		return 0;
	}
	while (!(h->msgs.Pop(msg))) {
		delay(20);
	}

	return 1;
}