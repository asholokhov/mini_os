/*  pair.h

    Implementation of pair container.
	
	Using:		
	1.	
		pair <_type1, _type2> p;
		p->first;
		p->second;
	
	2.
		make_pair(_type1 obj1, _type2 _obj2);

    Copyright (c) 2012 by MiniOSL.
    All Rights Reserved.
*/

#ifndef _PAIR_H_
#define _PAIR_H_

#include "mini_os\def.h"

template <class _First_Key, class _Second_Key>
	class pair {
	public:
		pair()
			:first(), second() { }
		pair(_First_Key fkey, _Second_Key skey)
			:first(fkey), second(skey) { }

		unsigned operator == (const pair<_First_Key, _Second_Key> &other) {
			return (first == other.first
				&& second == other.second);
		}
		~pair() { }

	private:
		_First_Key first;
		_Second_Key second;
	};




template<class _First_Key, class _Second_Key>
    inline pair<_First_Key, _Second_Key> 
    	make_pair(_First_Key x, _Second_Key y) { 
		
    		return pair<_First_Key, _Second_Key>(x, y); 
    	}
		
#endif