#include <dos.h>
#include <conio.h>
#include <stdio.h>
#include <graphics.h>
#include "mini_os\term.h"

#define PATH_TO_DRIVER "t:\\bgi\\"

static void StrToLower(char* str);
static void Interpretate(HANDLE h, const char* cmd);

// interface
static void TTerminal(HANDLE h, Window* wnd, const char* str);
static void TTime(HANDLE h, Window* wnd, const char* str);
static void TEcho(HANDLE h, Window* wnd, const char* str);
static void TCalculate(HANDLE h, Window* wnd, const char* str);
static void TUnknownCommand(HANDLE h, Window* wnd, const char* str);

// hard workers...
static double Calculate(const char* str);
static double Calc(const char* str, unsigned level, unsigned &index);

// ----------------------------------------------------------------------------

void Terminal() {
	HANDLE h = MGetProcHandle();
	Msg msg;
	unsigned run = 1;
	while (run) {
		if (!GetMessage(msg)) {
			break;
		}
		char buf[DATA_MAX_LEN];
		switch (msg.code) {
			case MSG_INPUT:
				if (!h->wnd.history.LastCommand(buf)) {
					break;
				}
				if (strcmp(buf, "exit") == 0) {
					run = 0;
					break;
				}
				else {
					Interpretate(h, buf);
				}

				break;

			case MSG_PAINT:
				h->wnd.Paint(msg.lparam);
				break;

			case MSG_CLOSE:
				run = 0;
				break;
		}
	}
}

// ----------------------------------------------------------------------------

void StrToLower(char* str) {
	for (int i = 0; i < strlen(str); i++) {
		if (str[i] >= 65 && str[i] <= 90) {
			str[i] += 32;
		}
	}
}

void Interpretate(HANDLE h, const char* cmd) {
	char buf[DATA_MAX_LEN];
	strcpy(buf, cmd);
	StrToLower(buf);
	Window* wnd = &h->wnd;

	if (0 == strcmp(buf, "terminal")
		|| 0 == strcmp(buf, "cmd")) {
		TTerminal(h, wnd, buf);
	}
	else if (0 == strcmp(buf, "time")) {
		TTime(h, wnd, buf);
	}
	else if (0 == strncmp(buf, "calc ", 5)) {
		TCalculate(h, wnd, buf);
	}
	else if (0 == strncmp(buf, "echo ", 5)) {
		TEcho(h, wnd, buf);
	}
	else if (0 == strcmp(buf, "bmp")) {
	/*
		int gd, gm, error;
		gd = DETECT;
		initgraph( &gd, &gm, PATH_TO_DRIVER);
		error = graphresult();
		if (error != grOk)
			printf("Graph error: %s", grapherrormsg(error));
		LoadBMP(0, 0, "111.bmp");
		closegraph();
	*/
	}
	else {
		TUnknownCommand(h, wnd, buf);
	}
}

// ----------------------------------------------------------------------------

void TTerminal(HANDLE h, Window* wnd, const char* str) {
	DCreateProcess(Terminal,
		0,
		"terminal",
		wnd->left + 1,
		wnd->top + 1,
		wnd->width,
		wnd->height,
		1,
		"Terminal");
}

void TTime(HANDLE h, Window* wnd, const char* str) {
	char buf[DATA_MAX_LEN];
	struct  time t;
	gettime(&t);
	sprintf(buf, "The current time is:");
	wnd->AddToHistory(buf, 0);
	sprintf(buf, "%2d:%02d:%02d", t.ti_hour, t.ti_min, t.ti_sec);
	wnd->AddToHistory(buf, 0);
}

void TEcho(HANDLE h, Window* wnd, const char* str) {
	char buf[DATA_MAX_LEN];
	if (strlen(str + 5)) {
		wnd->AddToHistory(str + 5, 0);
	}	
}
void TCalculate(HANDLE h, Window* wnd, const char* str) {
	char buf[DATA_MAX_LEN];
	sprintf(buf, "%lf", Calculate(str + 5));
	wnd->AddToHistory(buf, 0);
}

void TUnknownCommand(HANDLE h, Window* wnd, const char* str) {
	wnd->AddToHistory("Unknown command.", 0);
}

// ----------------------------------------------------------------------------

double Calculate(const char* str) {
	if (strlen(str) == 0) {
		return 0.0;
	}
	char buf[DATA_MAX_LEN];
	strcpy(buf, str);

	char* src = buf;
	char* dest = buf;
	while (*src) {
		if (*src != ' ') {
			*(dest++) = *src;
		}
		src++;
	}
	*dest = '\0';

	unsigned calcIndex = 0;
	return Calc(buf, 0, calcIndex);
}

double Calc(const char* str, unsigned level, unsigned &index) {
	int bracket = 0;
	unsigned count = 0;
	double lvalue;
	double rvalue;

	if ('(' == str[index]) {
		index++;
		lvalue = Calc(str, 0, index);
		if (')' != str[index++]) {
			return 0.0;
		}
	}
	else {
		sscanf(str + index, "%lf%n", &lvalue, &count);
		index += count;
	}

	while (1) {
		switch (str[index]) {
			case '+':
				if (level != 0) {
					return lvalue;
				}
				index++;
				rvalue = Calc(str, 0, index);
				return lvalue + rvalue;

			case '-':
				if (level != 0) {
					return lvalue;
				}
				index++;
				rvalue = Calc(str, 0, index);
				return lvalue - rvalue;

			case '*':
				index++;
				rvalue = Calc(str, 1, index);
				lvalue *= rvalue;
				break;

			case '/':
				index++;
				rvalue = Calc(str, 1, index);
				lvalue /= rvalue;
				break;

			case ')':
			case '\0':
			case '\n':
				return lvalue;

			default:
				index++;
				break;
		}
	}
	return lvalue;
}
