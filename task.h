/*  task.h

    Base Task implementation.
	

    Copyright (c) 2012 by MiniOSL.
    All Rights Reserved.
*/


#ifndef _TASK_H_
#define _TASK_H_

#include "mini_os\def.h"
#include "mini_os\queue.h"
#include "mini_os\msg.h"
#include "mini_os\wnd.h"
#include "mini_os\sem.h"

struct Task {
	char name[TASK_NAME_LEN];
	_segment ss;
	_offset sp;
	char* stack;
	int size;
	Queue<Msg> msgs;
	LockedMutex mutex;
	Window wnd;
};

typedef Task* HANDLE;

#endif