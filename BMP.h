/*  bmp.h

    BMP loading.
	
    Copyright (c) 2012 by MiniOSL.
    All Rights Reserved.
*/

#ifndef _BMP_H_
#define _BMP_H_

int LoadBMP(int x, int y, char * FileName);

#endif