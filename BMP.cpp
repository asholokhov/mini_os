#include <alloc.h>
#include <conio.h>
#include <graphics.h>
#include <stdio.h>
#include <stdlib.h>

#define BMP_UNABLE_OPEN_ERR 0
#define BMP_UNSUPPORTED_ERR 1
#define BMP_SUCCESSFUL_LOAD 2    

/*
  Function LoadBMP(x, y, FileName) realized load 16 bit
  image from disk. 
  Parametrs: x - offset by X
             y - offset by Y

  Note: for start work in graph mode, you must follow
   this steps: 1. Options->Linker->Graph Library (on)
               2. Copy (default: EGAVGA.BGI to BC\BIN\) video driver file
               3. ?????
               5. PROFIT!

  Example: 
    // ...
    int gd, gm, error;
    gd = DETECT;
    initgraph( & gd, & gm, "");
    error = graphresult();
    if (error != grOk) 
      printf("Graph error: %s", grapherrormsg(error));
    LoadBMP(0, 0, "tune.bmp");
    closegraph();
    // ...
*/

struct BMP {
    char Type[2];                   //File type. Set to "BM".
    unsigned long Size;             //Size in BYTES of the file.
    unsigned long Reserved;         //Reserved. Set to zero.
    unsigned long OffSet;           //Offset to the data.
    unsigned long headsize;         //Size of rest of header. Set to 40.
    unsigned long Width;            //Width of bitmap in pixels.
    unsigned long Height;           //Height of bitmap in pixels.
    unsigned int  Planes;           //Number of Planes. Set to 1.
    unsigned int  BitsPerPixel;     //Number of Bits per pixels.
    unsigned long Compression;      //Compression. Usually set to 0.
    unsigned long SizeImage;        //Size in bytes of the bitmap.
    unsigned long XPixelsPreMeter;  //Horizontal pixels per meter.
    unsigned long YPixelsPreMeter;  //Vertical pixels per meter.
    unsigned long ColorsUsed;       //Number of colors used.
    unsigned long ColorsImportant;  //Number of "important" colors.
};

int LoadBMP(int x, int y, char * FileName) {
    int a, b;
    BMP Img;

    unsigned char *pixels;
    int in = 0;
    unsigned char c = 0;
    
    FILE * fp = fopen(FileName, "rb");
    if (!fp) 
      return BMP_UNABLE_OPEN_ERR;

    fread( & Img, sizeof(Img), 1, fp);
    if (Img.BitsPerPixel != 4) {    // This isn't a 16 color bmp
        fclose(fp);
        return BMP_UNSUPPORTED_ERR;
    }

    fseek(fp, Img.OffSet, SEEK_SET);
    pixels = (unsigned char * )calloc(Img.Width / 2 + 1, sizeof(unsigned char));
    for (b = Img.Height; b >= 0; b--) {
        fread(pixels, sizeof(unsigned char), Img.Width / 2, fp);
        c  = 0; 
		in = 0;
		for (a = 0; a <= Img.Width; a += 2) {
			c = (pixels[in] | 0x00) >> 4;
			putpixel(a + x, b + y, c);
			c = (pixels[in] | 0xF0) & 0x0F;
			putpixel(a + 1 + x, b + y, c);
			in ++;
		}
    }
    free(pixels);
    fclose(fp);
    return BMP_SUCCESSFUL_LOAD;
}