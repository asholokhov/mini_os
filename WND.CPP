#include <dos.h>
#include <stdio.h>
#include <conio.h>

#include "mini_os\wnd.h"

void Window::Paint() {
	Paint(0);
}

void Window::Paint(unsigned lpar) {
	if (!visible) {
		return;
	}

	char sHoriz = 196;
	char sVert = 179;
	char fHoriz = (focused ? 205 : sHoriz);
	char fVert = (focused ? 186 : sVert);

	char ltCorner = (focused ? 201 : 218);
	char rtCorner = (focused ? 187 : 191);
	char lbCorner = (focused ? 200 : 192);
	char rbCorner = (focused ? 188 : 217);
	char trbTriangle = (focused ? 199 : 195);
	char tlbTriangle = (focused ? 182 : 180);
	char lrbTriangle = (focused ? 209 : 194);
	char lrtTriangle = (focused ? 193 : 193);

	char state;
	//state = GetCursorState();

	int oldRow = 0;
	int oldCol = 0;
	GetCursorPos(oldCol, oldRow);

	HideCursor();

	// input
	int historyLen = (history.GetSize() < height - 5 ? history.GetSize() : height - 5);
	if (0 == lpar) {
		for (int i = top + height - 2; i > top + 3 + historyLen; i--) {
			GotoXY(left + 1, i);
			cprintf("%-*s", width - 2, "");
		}

		GotoXY(left + 1, top + 3 + historyLen);
		cprintf("> %-*s", width - 4, input);


		int row = historyLen - 1;
		unsigned command;
		char buf[DATA_MAX_LEN];
		history.EnumerationBegin();
		while (row >= 0 &&history.EnumerationNext(buf, command)) {
			GotoXY(left + 1, top + 3 + row--);
			if (command) {
				cprintf("> %-*s", width - 4, buf);
			}
			else {
				cprintf("%-*s", width - 2, buf);
			}
		}
	}
	else if (1 == lpar || 2 == lpar) {
		GotoXY(left + 1, top + 3 + historyLen);
		cprintf("> %-*s", width - 4, input);
	}

	// top line
	GotoXY(left, top);
	putc(ltCorner, stdout);
	for (int i = 0; i < width - 6; i++) {
		putc(fHoriz, stdout);
	}
	cprintf("%c%c%c%c%c", lrbTriangle, fHoriz, fHoriz, fHoriz, rtCorner);

	// caption line
	if (0 == lpar) {
		GotoXY(left, top + 1);
		cprintf("%c %-*s %c X %c", fVert, width - 8, caption, sVert, fVert);
	}

	// mid line
	GotoXY(left, top + 2);
	putc(trbTriangle, stdout);
	for (i = 0; i < width - 6; i++) {
		putc(sHoriz, stdout);
	}
	cprintf("%c%c%c%c%c", lrtTriangle, sHoriz, sHoriz, sHoriz, tlbTriangle);

	// bottom line
	GotoXY(left, top + height - 1);
	putc(lbCorner, stdout);
	for (i = 0; i < width - 2; i++) {
		putc(fHoriz, stdout);
	}
	putc(rbCorner, stdout);

	// left line
	for (i = 0; i < height - 4; i++) {
		GotoXY(left, top + 3 + i);
		putc(fVert, stdout);
	}

	// right line
	for (i = 0; i < height - 4; i++) {
		GotoXY(left + width - 1, top + 3 + i);
		putc(fVert, stdout);
	}
	ShowCursor();
	//SetCursorState(state);
	if (focused) {
		SetCursorPos(left + 3 + strlen(input), top + 3 + historyLen);
	}
	else {
		SetCursorPos(oldCol, oldRow);
	}
}

void Window::AddToHistory(const char* str, unsigned command) {
	history.Push(str, command);
	Paint();
}

void Window::AddToHistory(unsigned command) {
	history.Push(input, command);
	strcpy(input, "");
	Paint();
}

void Window::Clear() {
	//char state = GetCursorState();
	HideCursor();
	for (int row = top; row < top + height; row++) {
		GotoXY(left, row);
		cprintf("%*s", width, "");
	}
	ShowCursor();
	//SetCursorState(state);
}

void InitCursor() {
	asm {
		mov ah, 01h
		//mov ch, 20h
		//mov cl, b
		//int 10h
	}
}

char GetCursorState() {
	char state;
	asm {
		mov ah, 03h
		int 10h
		mov byte ptr state, ch
	}
	return state;
}

void SetCursorState(char state) {
	asm {

		mov ah, 01h
		mov ch, byte ptr state
		int 10h
	}
}

void HideCursor() {
	asm {
		mov ah, 03h
		int 10h

		mov ah, 01h
		or ch, 00100000b
		int 10h
	}
}

void ShowCursor() {
	asm {
		mov ah, 03h
		int 10h

		mov ah, 01h
		and ch, 10011111b
		int 10h
	}
}
void SetCursorPos(int col, int row) {
	//row++;
	asm {
		mov ah, 02h
		xor bh, bh
		mov dl, byte ptr col
		mov dh, byte ptr row
		int 10h
	}
}
void GetCursorPos(int &col, int &row) {
	union REGS r;
	r.h.ah=3;
	r.h.bh=0;
	int86(0x10,&r,&r);
	col = r.h.dl;
	row = r.h.dh;
}

void GotoXY(unsigned x, unsigned y) {
	union REGS r;
	r.h.ah=2;
	r.h.dl=x;
	r.h.dh=y;
	r.h.bh=0;
	int86(0x10,&r,&r);
}
