/*  queue.h

    Implementation of Queue.
	

    Copyright (c) 2012 by MiniOSL.
    All Rights Reserved.
*/


#ifndef _QUEUE_H_
#define _QUEUE_H_

#include "mini_os\def.h"

template <class T>
class QItem {
public:
	QItem()
		:prev(0), next(0) { }
		
	QItem *prev;
	QItem *next;
	T obj;
};

template <class T>
class Queue{
public:
	Queue()
		:size(0), beg(0), end(0) { }
	~Queue() { }

	int GetSize();
	int Pop(T &obj);
	int Push(T obj);
	QItem<T>* GetFirst();

private:
	void FreeQItemAndNext(QItem<T>* beg);

private:
	QItem<T>* beg;
	QItem<T>* end;
	int size;
};


// ----------------------------------------------------------------
// IMPLEMENTATION

template <class T>
Queue<T>::Queue()
	:size(0), beg(0), end(0) {
	
}
	
template <class T> 
Queue<T>::~Queue() {
	if (size > 0) {
		FreeQItemAndNext(beg);
	}
}

template <class T>
void Queue<T>::FreeQItemAndNext(QItem<T>* QItem) {
	if (QItem) {
		FreeQItemAndNext(QItem->next);
		delete QItem;
	}
}

template <class T>
int Queue<T>::Pop(T &obj) {
	if (0 == size) {
		return 0;
	}
	
	obj = beg->obj;
	if (1 == size--) {
		delete end;
		beg = end = 0;
	}
	else {
		beg = beg->next;
		delete beg->prev;
		beg->prev = 0;
	}
	return 1;
}

template <class T>
int Queue<T>::Push(T obj) {
	if (size == 0) {
		beg = end = new QItem<T>();
		beg->next = beg->prev = 0;
	}
	else {
		QItem<T>* el = new QItem<T>();
		el->prev = end;
		el->next = 0;
		end->next = el;
		end = el;
	}
	end->obj = obj;
	size++;
	return 1;
}

template <class T>
QItem<T>* Queue<T>::GetFirst() {
	return beg;
}

template <class T>
int Queue<T>::GetSize() {
	return size;
}



#endif