#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

#include "mini_os\mini_os.h"


static unsigned _shutdown = 0;

// user functions
void Terminal();
void Notes();
// ----------------------------------------------------------------------------

void Notes() {
	HANDLE h = MGetProcHandle();
	Msg msg;
	unsigned run = 1;
	while (run) {
		if (!GetMessage(msg)) {
			break;
		}

		char buf[DATA_MAX_LEN];
		switch (msg.code) {
			case MSG_INPUT:
				if (!h->wnd.history.LastCommand(buf)) {
					break;
				}
				if(strcmp(buf, "exit") == 0) {
					run = 0;
				}
				break;
				
			case MSG_PAINT:
				h->wnd.Paint(msg.lparam);
				break;
				
			case MSG_CLOSE:
				run = 0;
				break;
		}
	}
}



int main(void) {
	srand(time(0));
	
	// user processes
	DCreateProcess(Terminal, 
		0, 
		"terminal", 
		1, 
		1, 
		35, 
		15, 
		1, 
		"Terminal");
	DCreateProcess(Notes, 
		0, 
		"notes", 
		40, 
		1, 
		35, 
		10, 
		1, 
		"Notes");
	
	StartSystem();
	

	ShutdownSystem();
	return 0;
}



/*
	Critical sections example.
*/
/*
void printMessage(int num) {
	cr_beg();
	for (int i = 0; i < 10; i++) {
		cprintf("Critical section: %d\r\n", num);
		delay(40);
	}
	cr_end();
}

void procOne() {
	printMessage(1);
}

void procTwo() {
	printMessage(2);
}

void procThree() {

}
*/

/*
	Message using example.
*/
/*
void procOne() {
	for (int i = 0; i < 10; i++) {
		delay(25);
		SendMessage("third", Msg(1, 0, 0));
	}
}

void procTwo() {
	for (int i = 0; i < 10; i++) {
		delay(25);
		SendMessage("third", Msg(2, 0, 0));
	}
	SendMessage("third", Msg(3, 0, 0));
}

void procThree() {
	delay(15);
	Msg msg;
	unsigned run = 1;
	while (run) {
		if (!GetMessage(msg)) {
			cprintf("No messages.\r\n");
			break;
		}

		cprintf("Message from %d process: (%d; %d; %d)\r\n",
			msg.code,
			msg.code,
			msg.lparam,
			msg.rparam);
		switch (msg.code) {
			case 3:
				cprintf("Shutdown...\r\n");
				run = 0;
				break;
			default:
				break;
		}
	}
}
*/

